# Bifrost.FPV

An open-source HD FPV solution based on commodity hardware.
Continues the evolution of the original wifibroadcast project, but abstracts the complexities so anybody can use it without loosing the strengths for the more experienced users.